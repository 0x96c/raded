package ga.alxme.raded.primitives;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;

import ga.alxme.raded.Primitive;

public class Box implements Primitive {
    private Rectangle box;

    public Box(float x, float y, float width, float height) {
        box = new Rectangle(x, y, width, height);
    }

    @Override
    public Rectangle getRectangle() {
        return box;
    }

    @Override
    public void setRectangle(Rectangle rect) {
        box = rect;
    }

    @Override
    public void render(ShapeRenderer renderer) {
        renderer.setColor(Color.GREEN);
        renderer.begin(ShapeRenderer.ShapeType.Line);
        renderer.rect(box.x, box.y, box.width, box.height);
        renderer.end();
    }
}
