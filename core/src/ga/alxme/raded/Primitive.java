package ga.alxme.raded;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;

public interface Primitive {
    Rectangle getRectangle();
    void setRectangle(Rectangle rect);
    void render(ShapeRenderer renderer);
}
