package ga.alxme.raded;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Gizmo {
    private Primitive target;
    private Rectangle targetRect;

    private Vector2 position;
    private Vector2 scale;

    private boolean isVisible;

    public Gizmo() {
        position = new Vector2(0, 0);
        scale = new Vector2(16, 4);

        isVisible = false;
    }

    public Primitive getTarget() {
        return target;
    }

    public void setTarget(Primitive target) {
        this.target = target;
        targetRect = target.getRectangle();

        position.set(targetRect.x, targetRect.y);
    }

    public void show() {
        if (!isVisible) {
            isVisible = true;
        }
    }

    public void hide() {
        if (isVisible) {
            isVisible = false;
        }
    }

    public boolean isAxis(Vector2 point) {
        Rectangle gizmo = new Rectangle(position.x, position.y, scale.x, scale.y);
        return gizmo.contains(point.x, point.y);
    }

    public void translate(float x, float y) {
        if (targetRect != null) {
            targetRect.x += x;
            targetRect.y += y;

            position.set(targetRect.x, targetRect.y);
        }
    }

    public void scale(float width, float height) {
        if (targetRect != null) {
            targetRect.width += width;
            targetRect.height += height;

            scale.set(targetRect.width, targetRect.height);
        }
    }

    public void render(ShapeRenderer renderer) {
        if (isVisible) {
            renderer.setColor(Color.WHITE);
            renderer.begin(ShapeRenderer.ShapeType.Filled);
            renderer.rect(position.x, position.y, scale.x, scale.y);
            renderer.rect(position.x, position.y, scale.y, scale.x);
            renderer.end();
        }
    }
}
