### What is RadEd? ###

**RadEd** is a two-dimensional level editor heavily influenced by [BUILD](http://wiki.eduke32.com/wiki/Build) and [GtkRadiant](http://icculus.org/gtkradiant/). It's written in Java and uses YAML to store information about geometry used in the map.

Please note that this is an experimental project I intially wanted to keep private. Therefore, do not expect everything to be clear and shiny.

### How to build RadEd? ###

It's very easy. **RadEd** uses *Gradle* to deal with dependencies, and is written in Java. Therefore, you must make sure you have *JRE* *7* and *Gradle* >=2.4 installed. It's also a good thing to have a Java IDE with Gradle support, such as IntelliJ IDEA or Eclipse.

### What about licensing? ###

**RadEd** is licensed under GNU GPL v3, meaning that you're free to fork, modify and create your own versions of this editor as long as you keep them free, open source and maintain the license. Note that the content you create in the editor is free of any licenses, so you may use it the way you like.