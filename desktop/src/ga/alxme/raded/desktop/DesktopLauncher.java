package ga.alxme.raded.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import ga.alxme.raded.RadEd;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "RadEd";
		config.resizable = false;
		config.width = 512;
		config.height = 512;
		new LwjglApplication(new RadEd(), config);
	}
}
