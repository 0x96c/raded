package ga.alxme.raded;

import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.math.Vector2;
import ga.alxme.raded.primitives.Box;

public class GridInput extends InputAdapter {
    private Grid grid;

    private Primitive currentObject;
    private Gizmo gizmo;

    boolean isGizmoSelected;

    public GridInput(Grid grid) {
        this.grid = grid;

        gizmo = new Gizmo();
        grid.addObject(gizmo);

        isGizmoSelected = false;
    }

    @Override
    public boolean keyUp(int keycode) {
        switch (keycode) {
            case Keys.FORWARD_DEL:
                if (currentObject != null) {
                    grid.cutObject(currentObject);
                }
                break;
        }

        return true;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if (gizmo.isAxis(grid.unproject(screenX, screenY))) {
            System.out.println("Axis!");
            isGizmoSelected = true;
        }
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        Vector2 point = grid.unproject(screenX, screenY);

        if (grid.isOccupied(point)) {
            currentObject = grid.getObject(point);
            gizmo.setTarget(currentObject);
            gizmo.show();
        } else {
            grid.addObject(new Box(point.x, point.y, 32, 32));
        }

        isGizmoSelected = false;

        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        System.out.println("Drag!");
        System.out.println(isGizmoSelected);
        if (isGizmoSelected) {
            gizmo.translate(32, 0);
        }

        return true;
    }
}
