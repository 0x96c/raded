package ga.alxme.raded;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.*;

public class RadEd extends ApplicationAdapter {
	private Grid grid;

	@Override
	public void create () {
		grid = new Grid(512, 512, 32, 32);

		Gdx.input.setInputProcessor(new GridInput(grid));
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		grid.render();
	}

	@Override
	public void dispose() {
		grid.dispose();
	}
}
