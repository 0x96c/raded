package ga.alxme.raded;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

import java.util.Vector;

public class Grid {
    private OrthographicCamera camera;

    private ShapeRenderer renderer;

    private Vector<Rectangle> cells;
    private Vector<Primitive> objects;

    private Gizmo gizmo;

    public Grid(int width, int height, int cellWidth, int cellHeight) {
        camera = new OrthographicCamera();
        camera.setToOrtho(false, width, height);
        camera.update();

        renderer = new ShapeRenderer();
        renderer.setProjectionMatrix(camera.combined);

        cells = new Vector<>();
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                cells.add(new Rectangle(x * cellWidth, y * cellHeight, cellWidth, cellHeight));
            }
        }

        objects = new Vector<>();
    }

    public Vector2 unproject(float x, float y) {
        Vector3 pos = new Vector3(x, y, 0);
        camera.unproject(pos);

        return new Vector2(pos.x, pos.y);
    }

    public boolean isOccupied(Vector2 point) {
        Rectangle rect = new Rectangle(point.x, point.y, 32, 32);

        for (Primitive object : objects) {
            if (rect.overlaps(object.getRectangle())) {
                return true;
            }
        }

        return false;
    }

    public void addObject(Primitive object) {
        Rectangle objectRect = object.getRectangle();

        for (Rectangle cell : cells) {
            if (objectRect.overlaps(cell)) {
                objectRect.setPosition(cell.x, cell.y);
                break;
            }
        }

        objects.add(object);
    }

    public void addObject(Gizmo gizmo) {
        this.gizmo = gizmo;
    }

    public Primitive getObject(Vector2 location) {
        for (Primitive object : objects) {
            if (object.getRectangle().contains(location.x, location.y)) {
                return object;
            }
        }

        return null;
    }

    public void cutObject(Primitive object) {
        objects.remove(object);
        gizmo.hide();
    }

    public void render() {
        camera.update();

        renderer.setColor(Color.GRAY);
        renderer.begin(ShapeRenderer.ShapeType.Line);
        for (Rectangle cell : cells) {
            renderer.rect(cell.x, cell.y,  cell.width, cell.height);
        }
        renderer.end();

        for (Primitive object : objects) {
            object.render(renderer);
        }

        gizmo.render(renderer);
    }

    public void dispose() {
        renderer.dispose();
    }
}
